<?php
ini_set('display_errors', true); 
error_reporting(E_ERROR);


$fk_name="key.txt";
$fs_name="shifr.txt";
$fd_name="deshifr.txt";

$fh_name="count_block.txt";//скрытый файл с хранением количества элементов шифрования
/////////////////////////////////////////////////////////////////////////
//Чтение файла ключа
/////////////////////////////////////////////////////////////////////////
$fk= fopen($fk_name, 'r');
    $size_key=filesize($fk_name);
    $fk_content=fread($fk, $size_key);
    fclose($fk);
    $fk_content=  str_split($fk_content);
    $count_fk_content=count($fk_content);
    $count_place=0;
    $thing=0;
    $t=0;
    for($i=0;$i<$count_fk_content;$i++){
        if($fk_content[$i]!=' ' && $thing==0 && $fk_content[$i]!='|'){//пока элемент массива не равен пробелу,скидываем всё во временный массив данных
            $tmp_massiv[$t]=$fk_content[$i];
            $t++;
        }elseif($fk_content[$i]==' ' && $thing==0){//если равен пробелу,то массив делаем числом и ставим как первый элемент супер возрастающей последовательности
            $massiv_super_high[$count_place]=implode($tmp_massiv);
            $count_place++;
            $t=0;
        }elseif($fk_content[$i]=='|' && $thing==0){  //если равен чёрточке,заполняем следующий элемент
            $thing++;
        }elseif ($fk_content[$i]!=' ' && $thing==1 && $fk_content[$i]!='|') {
            $q[$i]=$fk_content[$i];
        }elseif ($fk_content[$i]=='|' && $thing==1) {
            $q=  implode($q);
            $thing++;
        }elseif ($fk_content[$i]!=' ' && $thing==2 && $fk_content[$i]!='|') {
            $r[$i]=$fk_content[$i];
        }elseif ($fk_content[$i]=='|' && $thing==2) {
            $r=implode($r);
            $thing++;
        }elseif ($thing==3) {
            $i=$count_fk_content;
        }
    }
/////////////////////////////////////////////////////////////////////////
//чтение скрытого файла
/////////////////////////////////////////////////////////////////////////

    $fh=  fopen($fh_name, 'r');
    $size_h=  filesize($fh_name);
    $fh_content=fread($fh,$size_h);
    fclose($fh);
    $fh_content=  str_split($fh_content);
    $fh_count=  count($fh_content);
    for($i=0,$k=0,$t=0;$t<$fh_count;$t++,$i++){
        if($fh_content[$t]!='|'){
            $tmpp[$i]=$fh_content[$t];
        }else{
            $i=-1;
            $count_shifr_sum[$k]=implode($tmpp);
            $k++;
        }
    }
/////////////////////////////////////////////////////////////////////////
    //чтение шифрованного файла
    $fs= fopen($fs_name, 'r');
    $size_in=filesize($fs_name);
    $fs_content=fread($fs, $size_in);
    
        $fs_content=  str_split($fs_content);
    $fs_count=  count($fs_content);
    for($i=0,$k=0,$t=0;$t<$fs_count;$t++,$i++){
        if($fs_content[$t]!='|'){
            $tmpp[$i]=$fs_content[$t];
        }else{
            $i=-1;
            $shifr_sum[$k]=implode($tmpp);
            $k++;
            unset($tmpp);
        }
    }
    
    fclose($fs);
/////////////////////////////////////////////////////////////////////////
//Нахождение обратного числа r
/////////////////////////////////////////////////////////////////////////
for ($i=0; $i < $q; $i++) { 
	$ans=($i*$r)%$q;
	if($ans==1){
		$r_multi_reverse=$i;
	}
}
/////////////////////////////////////////////////////////////////////////
//получаем массив значений для расшифровки суммы
for($i=0;$i<count($shifr_sum);$i++){
    $reverse[$i]=($r_multi_reverse*$shifr_sum[$i])%$q;
}


/////////////////////////////////////////////////////////////////////////
$count_close_massiv=count($massiv_super_high);
$difference=1;

for($k=0;$k<count($shifr_sum);$k++){
    $difference=$reverse[$k];
    while($difference!=0){     
        for($i=$count_close_massiv,$t=0;$i>0;$i--,$t++){//for($i=$count_close_massiv;$i>0;$i--){
            if($difference<$massiv_super_high[$i-1]){
                $deshifr[$k][$i]=0;
            }elseif ($difference>=$massiv_super_high[$i-1]) {
                $deshifr[$k][$i]=1;
                $difference-=$massiv_super_high[$i-1];
            }
        }
    }
    if($count_shifr_sum[$k]<$count_close_massiv){
        for($u=$count_close_massiv;$count_shifr_sum[$k]!=$u;$u--)
        unset($deshifr[$k][$u]);
    }
    ksort($deshifr[$k]);
    $deshifr[$k]=  implode($deshifr[$k]);
}
$answer=implode($deshifr);


  ///////////////////////////////////////////////////////////////
   //Записываем дешифрованный файл
   ///////////////////////////////////////////////////////////////
   
   $fd=  fopen($fd_name, 'w');
   if(!file_exists($fd_name)){
       file_put_contents($fd, $answer);
   }  else {
       ftruncate($fd, 0);
       fwrite($fd, $answer);
   }
   fclose($fd);
   ///////////////////////////////////////////////////////////////
?>
        <table border="4">
            <tbody>
                <tr>
                    <td>Дешифрованный текст</td>
                </tr>
                <tr>
                    <td><?php echo $answer ?></td>
                </tr>
            </tbody>
        </table>